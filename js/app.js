'use strict';

/* App Module */

var spApp = angular.module('spApp', [
  'ngRoute',
  'ngAnimate',
  // 'ngCookie',
  'spControllers',
  'spFilters',
  'spServices'
]);

spApp.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'views/product-list.html',
      controller: 'ProductListCtrl'
    }).when('/edit/:prodId', {
      templateUrl: 'views/edit-product.html',
      controller: 'ProductEditCtrl'
    }).when('/create', {
      templateUrl: 'views/edit-product.html',
      controller: 'ProductCreateCtrl'
    }).when('/register', {
      templateUrl: 'views/reg.html',
      controller: 'UserRegisterCtrl'
    }).when('/login', {
      templateUrl: 'views/login.html',
      controller: 'UserLoginCtrl'
    }).when('/profile', {
      templateUrl: 'views/profile.html',
      controller: 'UserEditCtrl'
    }).when('/logout', {
      template: '',
      controller: 'UserLogoutCtrl'
    }).otherwise({
      templateUrl: 'views/404.html',
      controller: 'NoSuchPageCtrl'
    });
  }]);


spApp.run(['LoopBackAuth', '$rootScope', '$location', 'User', function (LoopBackAuth, $rootScope, $location, User) {

  $rootScope.currentUser = {id: LoopBackAuth.currentUserId};
  $rootScope.$on('$locationChangeStart', function (event, next, current) {
    // redirect to login page if not logged in and trying to access a restricted page
    var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
    var loggedIn = LoopBackAuth.currentUserId;
    $rootScope.currentUser = {id: LoopBackAuth.currentUserId};
    if (restrictedPage && !loggedIn) {
      $location.path('/login');
    }
  });

}]);