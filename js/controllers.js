'use strict';

/* Controllers */

var spControllers = angular.module('spControllers', []);

spControllers.controller('ProductListCtrl', ['$scope', 'Product',
  function ($scope, Product) {
    $scope.products = Product.query();
    $scope.orderProp = 'name';
    $scope.currentPage = 0;
    $scope.itemLimit = 10;
    $scope.changeLimit = function (e, limit) {
      $scope.itemLimit = limit;
      if ($scope.currentPage > $scope.numberOfPages()) {
        $scope.currentPage = $scope.numberOfPages() - 1;
      }
      e.preventDefault();
    };
    $scope.firstPage = function () {
      return $scope.currentPage == 0;
    };
    $scope.lastPage = function () {
      var lastPageNum = Math.ceil($scope.products.length / $scope.itemLimit - 1);
      return $scope.currentPage == lastPageNum;
    };
    $scope.numberOfPages = function () {
      return Math.ceil($scope.products.length / $scope.itemLimit);
    };
    $scope.startingItem = function () {
      return $scope.currentPage * $scope.itemLimit;
    };
    $scope.pageBack = function () {
      $scope.currentPage = $scope.currentPage - 1;
    };
    $scope.pageForward = function () {
      $scope.currentPage = $scope.currentPage + 1;
    };
    $scope.removeChecked = function () {
      $scope.products.forEach(function (el) {
        if (el.toRemove) {
          el.$remove({productId: el.id}).then(
            function (res) {
            },
            function (res) {
            });
        }
      })
    }
  }]);

spControllers.controller('ProductEditCtrl', ['$scope', '$routeParams', 'Product',
  function ($scope, $routeParams, Product) {
    $scope.welcomeMsg = 'Edit';
    $scope.prod = Product.get({productId: $routeParams.prodId}, function (prod) {
    });
    $scope.showMsg = {
      success: false,
      msg: ''
    };

    $scope.formSubmit = function (form) {
      $scope.showMsg.msg = '';
      $scope.prod.$update().then(function () {
        $scope.showMsg = {
          success: true,
          msg: 'Update success'
        };
      }, function () {
        $scope.showMsg = {
          success: false,
          msg: 'Update failed'
        };
      });
    };
  }]);
spControllers.controller('ProductCreateCtrl', ['$scope', '$location', 'Product',
  function ($scope, $location, Product) {
    $scope.welcomeMsg = 'Create';
    $scope.prod = new Product();
    $scope.showMsg = {
      success: false,
      msg: ''
    };

    $scope.formSubmit = function (form) {
      $scope.showMsg.msg = '';
      $scope.prod.$save().then(function (res) {
        $location.url('/edit/' + res.id);
      }, function (res) {
        $scope.showMsg = {
          success: false,
          msg: 'Create failed: ' + res.statusText
        };
      });
    };
  }]);

spControllers.controller('UserRegisterCtrl', ['$scope', '$location', 'User',
  function ($scope, $location, User) {
    $scope.welcomeMsg = 'Create';
    // $scope.user = new User();
    $scope.showMsg = {
      success: false,
      msg: ''
    };

    $scope.formSubmit = function (form) {
      $scope.showMsg.msg = '';
      User.create($scope.user).$promise
        .then(function (res) {
          $location.url('/login');
        }, function (res) {
          $scope.showMsg = {
            success: false,
            msg: 'Create failed: ' + res.statusText
          };
        });
    };
  }]);

spControllers.controller('UserEditCtrl', ['$scope', '$rootScope', '$location', 'User', 'LoopBackAuth',
  function ($scope, $rootScope, $location, User, LoopBackAuth) {
    $scope.user = User.getCurrent();
    $scope.showMsg = {
      success: false,
      msg: ''
    };
    $scope.ph_numbr = /^(\+?(\d{1}|\d{2}|\d{3})[- ]?)?\d{3}[- ]?\d{3}[- ]?\d{4}$/;
    console.log($scope.user);

    $scope.checkPass = function (e) {
      var firstPass = $scope.UserForm.passwordFirst;
      var confirmtPass = $scope.UserForm.passwordRepeat;
      var check1 = firstPass.$invalid || firstPass.$pristine;
      var check2 = confirmtPass.$invalid || confirmtPass.$pristine;
      console.log(check1 || check2);
      return check1 || check2;
    };
    $scope.changePass = function (e) {
      $scope.user.password = $scope.UserForm.passwordFirst;
      User.resetPassword($scope.user).$promise.then(function (res) {
        console.log(res);
      });
    };
    $scope.formSubmit = function (form) {
      console.log($scope.user);
      User.upsert($scope.user).$promise.then(function (res) {
        console.log(res);
      });
      // $scope.user.$save().then(function (res) {
      //     $location.url('/edit/' + res.id);
      // }, function (res) {
      //     $scope.showMsg = {
      //         success: false,
      //         msg: 'Create failed: ' + res.statusText
      //     };
      // });
    };
  }]);

spControllers.controller('UserLoginCtrl', ['$scope', '$rootScope', '$location', 'User','userService',
  function ($scope, $rootScope, $location, User, userService) {
    $scope.showMsg = {
      success: false,
      msg: ''
    };

    $scope.formSubmit = function (form) {
      $scope.showMsg.msg = '';
      var params = {rememberMe: $scope.user.rememberMe};
      User.login(params, $scope.user).$promise.then(function (res) {
        userService.setUser(res.userId);
        userService.setUserEmail(res.user.email);
        $location.url('/');
      }, function (res) {
        if (res.status === 401) {
          $scope.showMsg = {
            success: false,
            msg: 'Wrong email or password'
          };
        }
      });
    };
  }]);


spControllers.controller('UserLogoutCtrl', ['$scope', '$rootScope', '$location', 'User','userService',
  function ($scope, $rootScope, $location, User, userService) {
    User.logout().$promise.then(function (res) {
      userService.setUser(null);
      userService.setUserEmail(null);
      $location.url('/');
    }, function (res) {
      console.log(res);
    })
  }]);

spControllers.controller('UserActionsCtrl', ['$scope', '$rootScope', 'User', 'userService',
  function ($scope, $rootScope, User, userService) {
    $scope.Service = userService;
    $scope.user = User.getCurrent().$promise.then(function (e) {
      $scope.Service.setUserEmail(e.email);
    });
  }]);

spControllers.controller('NoSuchPageCtrl', ['$scope',
  function ($scope) {

  }]);
