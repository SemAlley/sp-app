module.exports = function(app) {
  app.dataSources.mysqlDs.automigrate('Products', function(err) {
    if (err) throw err;

    var tmp = [];

    for (var i = 0; i < 100; i++) {
      tmp[i] = {
        name: 'product ' + i,
        price: (Math.random() * 10000).toFixed(2),
        description: 'product ' + i + ' description'
      };
    }

    app.models.Products.create(tmp, function(err, Products) {
      if (err) throw err;

      console.log('Models created: \n', Products);
    });
  });
};