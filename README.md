# Test task for Angular developer

## Goal

Создать single-page application, со следующей структурой:

### Structure

- Страница авторизации
    - email - valid email address
    - password - 6-16 characters [a-zA-Z0-9]
    - remember me - checkbox

- После авторизации попадаем на индексную страницу
    - Информация о пользователе
        - Имя/email
        - Кнопка “настройки”
        - Кнопка “logout” возвращает на страницу авторизации

    - Список товаров
        - No п/п
        - Наименование
        - Цена - число в формате 1234,56
        - Кнопка “Подробнее”/”скрыть” - должна раскрыть/cкрыть описание товара (открыто может быть только одно описание, при нажатии кнопки “подробнее” другого товара раскрытое описание скрывается)
        - создать товар
        - редактировать товар
        - checkbox для пометки товара
        - Возможность группового удаления помеченных товаров товаров

    - Постраничная навигация с возможностью выбора количества товаров на странице (10-20-50)

- Создание/редактирование товара
    - Наименование - maxlength 255, required
    - Цена - число в формате 1234,56, required
    - Описание - maxlength 1000, required
    - Кнопка создать/сохранить

- Страница редактирования профиля
    - Имя - maxlength 255
    - Фамилия - maxlength 255
    - email- valid email address, required
    - Номер телефона в одном из форматов, например +380XXХХХХХХХ
    - Кнопка “Сохранить”
    - Кнопка “change password” открывает форму смены пароля
        - password - 6-16 characters [a-zA-Z0-9]
        - repeat password - 6-16 characters [a-zA-Z0-9] equals password
        - Кнопка “Сохранить”

- Страница 404

## Install

### В директиве root-dir(sp-app)/serve
- Нужен node
- **Console:** npm install
- **Console:** npm install -g strongloop
- server/datasources.json ввести данные к базе
- server/ **Console** node create-lb-tables.js
- root-dir(sp-app)/serve **Console:** slc run (if some trouble with StrongLoop use sp-app/serve/server node server.js )

### В корневой папке:
- Нужен node & bower
- **Console:** npm install
- **Console:** npm start